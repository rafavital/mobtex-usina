using Mobtex.Utilities.ExtendedUnityEvents;
using UnityEngine;

namespace Mobtex.NuclearSim.ReactorManager
{
    public class ReactorManager : MonoBehaviour
    {
        [Header("Height")]
        [SerializeField] private float heightCurrent = 180; // abstract height unit 

        [Header("Power")]
        [SerializeField] private float powerCurrent = 180f; // degrees

        [Tooltip("The desired angle for the power")]
        [SerializeField] private float powerDesired = 160f; // degrees

        [Tooltip("The allowed range for the power")]
        [SerializeField] private float powerAllowedRange = 40f; // degrees

        [Header("Temperature")]
        [SerializeField] private float temperatureCurrent = 180f; // degrees

        [Tooltip("The allowed range for the temperature")]
        [SerializeField] private float temperatureAllowedRange = 40f; // degrees

        [Tooltip("The angle that denotes an overheat")]
        [SerializeField] private float overheatTemperature = 330f; // degrees

        [Header("Water Flow")]
        [SerializeField] private float waterFlowCurrent = 180f; // degrees

        [Header("Unity Events")]

        // [Tooltip("Raises when the power needle enter the desired range")]
        // public UnityEvent onEnterDesiredPower = null;

        // [Tooltip("Raises when the power needle leaves the desired range")]
        // public UnityEvent onExitDesiredPower = null;

        [Tooltip("Raises when the temperature changes")]
        public FloatEvent onChangeTemperature = null;

        public FloatEvent onChangeDesiredPower = null;
        public FloatEvent onChangeCurrentPower = null;

        public BoolEvent onScoring = null;

        [SerializeField] private float temperatureGoal; // degrees
        private Vector2 powerRandomRange = new Vector2(20, 320);

        #region UNITY_CALLS

        private void Start()
        {
            SetTemperature(180);
            SetPower(180);
            SetDesiredPower(160);
            SetWaterFlow(180);
            SetHeight(180);
        }

        private void Update()
        {
            var tempDiff = temperatureGoal - temperatureCurrent;
            if (Mathf.Abs(tempDiff) > 0.1f)
            {
                SetTemperature(temperatureCurrent + 20 * Mathf.Sign(tempDiff) * Time.deltaTime);
            }
        }

        #endregion

        #region PUBLIC_METHODS

        public void SetHeight(float newHeight)
        {
            if (newHeight == heightCurrent)
                return;

            heightCurrent = Mathf.Clamp(newHeight, 0, 400);
            UpdateTemperatureGoal();
            SetPower(Mathf.Lerp(0f, 360f, newHeight / 400f));
        }
        public void SetWaterFlow(float newWaterFlow)
        {
            if (newWaterFlow == waterFlowCurrent)
                return;

            waterFlowCurrent = newWaterFlow;
            UpdateTemperatureGoal();
        }
        public void GetNewPowerGoal()
        {
            powerDesired = Random.Range(powerRandomRange.x, powerRandomRange.y);
            onChangeDesiredPower.Invoke(powerDesired);
        }

        #endregion

        #region PRIVATE_METHODS

        private void SetPower(float newPower)
        {
            powerCurrent = newPower;
            onChangeCurrentPower.Invoke(newPower);
            onScoring.Invoke(powerCurrent > powerDesired && powerCurrent < powerDesired + powerAllowedRange);
        }
        private void SetDesiredPower(float desiredPower) => powerDesired = desiredPower;
        private void SetTemperature(float newTemperature)
        {
            temperatureCurrent = newTemperature;
            onChangeTemperature.Invoke(temperatureCurrent);
        }

        private void UpdateTemperatureGoal()
        {
            var t = (2f * heightCurrent - waterFlowCurrent) / 440f;
            temperatureGoal = Mathf.Lerp(0f, 360f, t);
        }

        #endregion
    }
}