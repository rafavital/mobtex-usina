using Mobtex.Utilities;
using Mobtex.Utilities.ExtendedUnityEvents;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int score;
    [SerializeField] private bool scoring;
    private Timer timer;
    public bool Scoring
    {
        get => scoring;
        set
        {
            scoring = value;
        }
    }
    public FloatEvent onUpdateScore = null;
    private void Start()
    {
        timer = new Timer(1);
        timer.onTimeout += HandleTimeout;
    }
    private void Update()
    {
        if (scoring)
            timer.Tick(Time.deltaTime);
    }
    public void RestartGame() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    [ContextMenu("End Game")]
    public void EndGame()
    {
        print("Game Over");
        Time.timeScale = 0;
    }

    private void HandleTimeout()
    {
        score += 2;
        onUpdateScore.Invoke(score);
        timer.Reset();
    }
}
