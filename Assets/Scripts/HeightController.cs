using UnityEngine;
using Mobtex.Utilities.ExtendedUnityEvents;

namespace Mobtex.NuclearSim.HeightControl
{
    public class HeightController : MonoBehaviour
    {
        [SerializeField] private Vector2 heightRange = new Vector2(0, 400);

        [SerializeField] private FloatEvent onHeightChange;


        private void Start()
        {
            SetHeight(0.45f);
        }

        public void SetHeight(float percentage)
        {
            onHeightChange.Invoke(Mathf.Lerp(heightRange.y, heightRange.x, percentage));
        }
    }
}