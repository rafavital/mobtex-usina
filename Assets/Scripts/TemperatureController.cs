using UnityEngine;

public class TemperatureController : MonoBehaviour
{
    [SerializeField] private RectTransform needlePivot;

    public void UpdateTemperatureNeedle(float angle)
    {
        needlePivot.eulerAngles = new Vector3(0, 0, -angle);
    }
}
