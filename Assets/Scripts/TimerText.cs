using TMPro;
using UnityEngine;

public class TimerText : MonoBehaviour
{
    [SerializeField] private TMP_Text timerText;
    public Color cyan = new Color(182, 94, 100);
    public Color yellow = new Color(41, 77, 99);
    public Color red = new Color(100, 94, 100);
    public void UpdateText(float time)
    {
        timerText.text = Mathf.RoundToInt(time).ToString();
        if (time / 120f > 0.5f)
            timerText.color = Color.Lerp(cyan, yellow, time / 120f);
        else
            timerText.color = Color.Lerp(yellow, red, time / 120f);

    }
}
