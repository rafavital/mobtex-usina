using UnityEngine;
using TMPro;

public class ScoreController : MonoBehaviour
{
    [SerializeField] private TMP_Text scoreText;

    public void UpdateScore(float score) => scoreText.text = score.ToString();
}
