using UnityEngine;
using Mobtex.Utilities.ExtendedUnityEvents;

namespace Mobtex.NuclearSim.Crank
{
    public class CrankController : MonoBehaviour
    {
        public FloatEvent onCrankRotated = null;
        public FloatEvent onCrankRotatedNormalized = null;
        private bool handleHeld;
        public bool HandleHeld { get => handleHeld; set => handleHeld = value; }

        [SerializeField] private float crankAngle;
        public float CrankAngle
        {
            get => crankAngle;
            set
            {
                crankAngle = value;
                rotationPivot.eulerAngles = new Vector3(0, 0, 90f - CrankAngle);
                onCrankRotated.Invoke(crankAngle);
                onCrankRotatedNormalized.Invoke(crankAngle / 360f);
            }
        }
        [SerializeField] private RectTransform rotationPivot;
        private Camera mainCam;
        private Vector2 mousePos, pivot;

        private void OnDrawGizmos()
        {
            Debug.DrawLine(pivot, mousePos);
        }

        private void Start()
        {
            CrankAngle = 180f;
            mainCam = Camera.main;
        }

        private void Update()
        {
            if (handleHeld)
            {
                mousePos = (Vector2)Input.mousePosition;
                pivot = rotationPivot.position;
                var dir = mousePos - pivot;
                dir.Normalize();

                // print($"Mouse: {mousePos} | Trans: {pivot} | dir: {dir}");
                var desiredAngle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
                var normalizedAngle = desiredAngle <= 0 ? 360f + desiredAngle : desiredAngle;
                // normalizedAngle = Mathf.Clamp(normalizedAngle, 0.1f, 359.9f);
                CrankAngle = normalizedAngle;



            }
        }
    }
}