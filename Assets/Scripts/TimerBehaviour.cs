using Mobtex.Utilities.ExtendedUnityEvents;
using UnityEngine;
using UnityEngine.Events;

namespace Mobtex.Utilities
{
    public class TimerBehaviour : MonoBehaviour
    {
        [SerializeField] private float duration;
        [SerializeField] private bool repeat;
        [SerializeField] private bool autoStart;
        [SerializeField] private UnityEvent onTimeout = null;
        [SerializeField] private FloatEvent onUpdateTime = null;
        private Timer timer;
        private bool running = false;
        private void Start()
        {
            running = false;
            timer = new Timer(duration);
            timer.onTimeout += HandleTimeout;

            if (autoStart)
                StartTimer();
        }

        private void Update()
        {
            if (running)
            {
                timer.Tick(Time.deltaTime);
                onUpdateTime.Invoke(timer.remainingDuration);
            }
        }

        public void StartTimer() => running = true;
        public void StopTimer() => running = false;
        public void ResetTimer() => timer.Reset();
        private void HandleTimeout()
        {
            onTimeout.Invoke();

            if (repeat)
                ResetTimer();
            else
                StopTimer();
        }

    }
}