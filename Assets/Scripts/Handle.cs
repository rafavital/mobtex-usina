using UnityEngine;
using UnityEngine.EventSystems;
using Mobtex.Utilities.ExtendedUnityEvents;

namespace Mobtex.NuclearSim.Crank
{
    public class Handle : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public BoolEvent onHeld = null;
        public void OnPointerDown(PointerEventData eventData) => onHeld.Invoke(true);
        public void OnPointerUp(PointerEventData eventData) => onHeld.Invoke(false);
    }

}