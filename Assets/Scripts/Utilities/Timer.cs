using System;

namespace Mobtex.Utilities
{
    public class Timer
    {
        public event Action onTimeout;
        public float remainingDuration;
        public float duration;

        public void Tick(float delta)
        {
            if (remainingDuration == 0f)
                return;

            remainingDuration -= delta;

            if (remainingDuration <= 0f)
            {
                remainingDuration = 0f;
                onTimeout?.Invoke();
            }
        }

        public void Reset() => remainingDuration = duration;

        public Timer(float duration)
        {
            this.duration = remainingDuration = duration;
        }
    }
}