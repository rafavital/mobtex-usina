using UnityEngine.Events;

namespace Mobtex.Utilities.ExtendedUnityEvents
{
    [System.Serializable] public class BoolEvent : UnityEvent<bool> { }
    [System.Serializable] public class FloatEvent : UnityEvent<float> { }
}