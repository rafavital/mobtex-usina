using UnityEngine;

public class PowerController : MonoBehaviour
{
    [SerializeField] private RectTransform desiredPowerIndicator;
    [SerializeField] private RectTransform needlePivot;

    public void UpdateDesiredPower(float angle)
    {
        desiredPowerIndicator.eulerAngles = new Vector3(0, 0, angle);
    }

    public void UpdateNeedle(float angle)
    {
        needlePivot.eulerAngles = new Vector3(0, 0, -angle);
    }
}
